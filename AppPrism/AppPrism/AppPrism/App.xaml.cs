﻿using Prism.Unity;
using AppPrism.Views;
//using AppPrism.Views.MasterDetails;
using Xamarin.Forms;

namespace AppPrism
{
    public partial class App : PrismApplication
    {
        public App(IPlatformInitializer initializer = null) : base(initializer) { }

        protected override void OnInitialized()
        {
            InitializeComponent();

            //NavigationService.NavigateAsync("MainPage?title=LBP");
            NavigationService.NavigateAsync("PageAccueil");
        }

        protected override void RegisterTypes()
        {
            Container.RegisterTypeForNavigation<MainPage>();
            Container.RegisterTypeForNavigation<CategorieAffichage>();
            Container.RegisterTypeForNavigation<CategoriePage>();
            Container.RegisterTypeForNavigation<Authentication>();
            Container.RegisterTypeForNavigation<MasterPage>();
            Container.RegisterTypeForNavigation<MasterNavigation>();
            Container.RegisterTypeForNavigation<FluxRss>();
            Container.RegisterTypeForNavigation<PageAccueil>();
        }
    }
}
