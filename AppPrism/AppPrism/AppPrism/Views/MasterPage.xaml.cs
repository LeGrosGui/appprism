﻿using System;
using Prism.Navigation;
using Xamarin.Forms;

namespace AppPrism.Views
{
    public partial class MasterPage : MasterDetailPage, IMasterDetailPageOptions
    {
        public bool IsPresentedAfterNavigation
        {
            get
            {
                return Device.Idiom != TargetIdiom.Phone;
            }
        }

        public MasterPage()
        {
            InitializeComponent();
        }

    }
}