﻿using System;
using Prism.Navigation;
using Xamarin.Forms;

namespace AppPrism.Views
{
    public partial class MasterNavigation : NavigationPage, INavigationPageOptions
    {
        public MasterNavigation()
        {
            InitializeComponent();
        }

        public bool ClearNavigationStackOnNavigation
        {
            get
            {
                return true;
            }
        }
    }
}
