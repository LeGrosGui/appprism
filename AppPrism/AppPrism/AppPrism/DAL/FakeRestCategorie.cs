﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using AppPrism.Model;

namespace AppPrism.DAL
{
    public class FakeRestCategorie : IRestCategorie
    {
        public Task DeleteCategorie(string ID)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Categorie>> GetCategorie()
        {
            List<Categorie> poney = new List<Categorie>();
            poney.Add(new Categorie { Name = "Lucius", Description = "Unedescription" });
            poney.Add(new Categorie { Name = "Marche", Description = "DescriptionQuiMarche" });
            await Task.Delay(200);
            return poney;

        }

        public Task SaveCategorie(Categorie categorie, bool isNew = false)
        {
            throw new NotImplementedException();
        }
    }
}
