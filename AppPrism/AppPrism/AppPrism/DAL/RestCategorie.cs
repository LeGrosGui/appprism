﻿using AppPrism.DAL;
using AppPrism.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AppPrism.DAL
{
    public class RestCategorie : IRestCategorie, IDisposable
    {
       HttpClient client;


        public RestCategorie()
        {
            this.client = new HttpClient();
        }

        public async Task<List<Categorie>> GetCategorie()
        {
            
            List<Categorie> categories = new List<Categorie>();
            Uri uri = new Uri(string.Format(Constants.RestUrl,string.Empty));

            var response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                categories = JsonConvert.DeserializeObject<List<Categorie>>(content);
                return categories;
            }

            categories.Add(new Categorie { Name = "TEST" });
            categories.Add(new Categorie { Name = "PONEY" });
            return categories;
        }


        public async Task SaveCategorie(Categorie categorie, bool isNew = false)
        {
            string trans = "/" + categorie.ID;
            Uri uri = new Uri(string.Format(Constants.RestUrl,trans));

            try
            {
                var json = JsonConvert.SerializeObject(categorie);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;

                if (isNew)
                {
                    response = await client.PostAsync(uri, content);
                }
                else
                {
                    response = await client.PutAsync(uri, content);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task DeleteCategorie(string ID)
        {
            string trans = "/" + ID;
            Uri uri = new Uri(string.Format(Constants.RestUrl, trans));

            try
            {
                var response = await client.DeleteAsync(uri);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Dispose()
        {
            
        }
    }
}
