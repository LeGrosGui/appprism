﻿using AppPrism.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AppPrism.DAL
{
    class AuthentificationService
    {
        HttpClient client;

        public AuthentificationService()
        {
            this.client = new HttpClient();
        }

        public async Task<bool> Authentification(Authentication auth)
        {
            
            string url = "https://*-webservices.klanik.com/KlanikWebService.svc/user/json/Authenticate/";

            Uri uri = new Uri(string.Format(url));
            HttpResponseMessage response = null;
            try
            {
                var json = JsonConvert.SerializeObject(auth);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                response = await client.PostAsync(uri, content);
            }
            catch (Exception)
            {
                throw;
            }
            if (response.IsSuccessStatusCode)
            {
                return true;
            }else
            {
                return false;
            }
        }
    }


}
