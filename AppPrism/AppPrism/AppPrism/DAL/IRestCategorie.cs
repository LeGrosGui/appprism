﻿using AppPrism.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppPrism.DAL
{
    public interface IRestCategorie
    {
        Task<List<Categorie>> GetCategorie();

        Task SaveCategorie(Categorie categorie, bool isNew = false);

        Task DeleteCategorie(string ID);

    }
}