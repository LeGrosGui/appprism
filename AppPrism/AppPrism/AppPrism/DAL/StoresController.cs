﻿using AppPrism.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AppPrism.DAL
{
    public class StoresController
    {
        HttpClient client;

        public StoresController()
        {
            this.client = new HttpClient();
        }

        public async Task<List<HotDogStore>> GetAllStore()
        {
            List<HotDogStore> storeList = new List<HotDogStore>();

            Uri uri = new Uri(string.Format(Constants.ApiUrl, "stores/GetAll"));

            var response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                storeList = JsonConvert.DeserializeObject<List<HotDogStore>>(content);
                return storeList;
            }
            return storeList;
        }


        public async Task<HotDogStore> GetMyStore(string AccessToken)
        {
            HotDogStore myStore = new HotDogStore();

            Uri uri = new Uri(string.Format(Constants.ApiUrl, "stores/GetMyStore"));
            //client.SetBearerToken(AccessToken);

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);

            var response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                myStore = JsonConvert.DeserializeObject<HotDogStore>(content);
                return myStore;
            }
            return myStore;
        }


        public async Task AddStore(HotDogStore hotDogStore)
        {

            Uri uri = new Uri(string.Format(Constants.ApiUrl,"store/AddStore"));
            //client.SetBearerToken(AccessToken);

            try
            {
                var json = JsonConvert.SerializeObject(hotDogStore);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;
                response = await client.PostAsync(uri, content);

            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task DeleteHotDog(string ID)
        {
            string trans = "HotDogs/Delete/" + ID;

            Uri uri = new Uri(string.Format(Constants.ApiUrl,trans));

            try
            {
                var response = await client.DeleteAsync(uri);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public async Task<List<HotDog>> GetHotDogByStore(int IdStore)
        {
            List<HotDog> HotDogsList = new List<HotDog>();

            string trans = "HotDogs/GetByStoreId/" + IdStore;
            Uri uri = new Uri(string.Format(Constants.ApiUrl,trans));

            var response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                HotDogsList = JsonConvert.DeserializeObject<List<HotDog>>(content);
                return HotDogsList;
            }
            return HotDogsList;
        }


        public async Task AddHotDog(int IdStore, HotDogStore hotDogStore)
        {
            string trans = "HotDogs/Add/" + IdStore;
            Uri uri = new Uri(string.Format(Constants.ApiUrl, trans));

            try
            {
                var json = JsonConvert.SerializeObject(hotDogStore);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;
                response = await client.PostAsync(uri, content);

            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
