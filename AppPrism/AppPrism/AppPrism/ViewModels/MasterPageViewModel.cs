﻿using AppPrism.Model;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace AppPrism.ViewModels
{
    public class MasterPageViewModel : ViewModelBase
    {
        INavigationService _navigationService;
        public DelegateCommand<string> Navigate { get; }

        
        private MasterNavClass _selectedNavigation;


        private ObservableCollection<MasterNavClass> _firstListNavigation;
        public ObservableCollection<MasterNavClass> FirstListNavigation
        {
            get { return _firstListNavigation; }
            set { SetProperty(ref _firstListNavigation, value); }
        }

        private ObservableCollection<MasterNavClass> _secondListNavigation;
        public ObservableCollection<MasterNavClass> SecondListNavigation
        {
            get { return _secondListNavigation; }
            set { SetProperty(ref _secondListNavigation, value); }
        }

        private ObservableCollection<MasterNavClass> _thirdListNavigation;
        public ObservableCollection<MasterNavClass> ThirdListNavigation
        {
            get { return _thirdListNavigation; }
            set { SetProperty(ref _thirdListNavigation, value); }
        }

        public MasterPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            Navigate = new DelegateCommand<string>(NavigateCommandWithReset);

            FirstListNavigation = new ObservableCollection<MasterNavClass>
            {
                new MasterNavClass { Name = "Accueil", Destination = "MasterNavigation/FluxRss", ImageSource = "Conges.png" },
                new MasterNavClass { Name = "DashBoard", Destination = "MasterNavigation/MainPage", ImageSource = "Dashboard.png" },
                new MasterNavClass { Name = "Categorie", Destination = "MasterNavigation/CategorieAffichage", ImageSource = "Conversation.png" }
            };

            SecondListNavigation = new ObservableCollection<MasterNavClass>
            {
               new MasterNavClass { Name = "Login", Destination = "MasterNavigation/Authentication", ImageSource = "Login.png" }
            };

            ThirdListNavigation = new ObservableCollection<MasterNavClass>
            {
               new MasterNavClass { Name = "Support IT", Destination = "MasterNavigation/Authentication", ImageSource = "SupportIT.png" }
            };
        }

        private void NavigateCommand(string obj)
        {
            _navigationService.NavigateAsync(obj);
        }

        private void NavigateCommandWithReset(string obj)
        {
            SelectedNavigation = new MasterNavClass { Name = "Flux Rss", Destination = obj, ImageSource = "LogoKlanik.png" };
        }

        public override void OnNavigatedFrom(NavigationParameters parameters)
        {
            base.OnNavigatedFrom(parameters);
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
        }

        public MasterNavClass SelectedNavigation
        {
            get { return _selectedNavigation; }
            set
            {
                SetProperty(ref _selectedNavigation, value);
                NavigateCommand(_selectedNavigation.Destination);
            }
        }
    }
}
