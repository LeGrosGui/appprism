﻿using AppPrism.DAL;
using AppPrism.Model;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AppPrism.ViewModels
{
    public class AuthenticationViewModel : BindableBase
    {
        INavigationService _navigationService;
        public DelegateCommand Connexion { get; set; }

        public AuthenticationViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            Connexion = new DelegateCommand(ConnexionCommand, CanExecute);
        }


        private void ConnexionCommand()
        {

            Test = Login + Password;
            //AuthentificationService service = new AuthentificationService();
            //Authentication auth = new Authentication { Login = "UnPoney", Password = "0000" };
            //bool yes= await service.Authentification(auth);
            //_navigationService.NavigateAsync("MainPage");
        }

        private bool CanExecute()
        {
            if (!string.IsNullOrEmpty(Login) && !string.IsNullOrEmpty(Password))
                return true;
            return false;
        }

        private string _login;
        public string Login
        {
            get { return _login; }
            set {
                SetProperty(ref _login, value);
                Connexion.RaiseCanExecuteChanged();
            }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set {
                SetProperty(ref _password, value);
                Connexion.RaiseCanExecuteChanged();
            }
        }

        private string _test;
        public string Test
        {
            get { return _test; }
            set { SetProperty(ref _test, value); }
        }

    }
     
}