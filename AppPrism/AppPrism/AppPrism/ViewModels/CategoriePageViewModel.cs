﻿using AppPrism.Model;
using AppPrism.DAL;
using AppPrism.ViewModels;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AppPrism.ViewModels
{
    public class CategoriePageViewModel : BindableBase, INavigationAware
    {
        INavigationService _navigationService;
        IRestCategorie rest;
        private Categorie _cateElement;
        public DelegateCommand OnSave { get; }
        public DelegateCommand OnDelete { get; }
        public DelegateCommand OnRetour { get; }
        private bool _isAllow = true;
        private bool _isWorking;
        private bool isNew { get; set; }




        public CategoriePageViewModel(INavigationService navigationService)
        {
            isNew = false; 
            rest = new RestCategorie();
            _navigationService = navigationService;
            OnSave = new DelegateCommand(OnSaveCommand).ObservesCanExecute((p) => IsAllow);
            OnDelete = new DelegateCommand(OnDeleteCommand).ObservesCanExecute((p) => IsAllow);
            OnRetour = new DelegateCommand(OnRetourCommand);
            _isWorking = false;
        }


        private async void OnDeleteCommand()
        {
            IsWorking = true; IsAllow = false; 
            await rest.DeleteCategorie(CateElement.ID.ToString());
            IsAllow = true; IsWorking = false;
            await _navigationService.GoBackAsync();
        }

        private async void OnSaveCommand()
        {
            IsAllow = false; IsWorking = true;
            await rest.SaveCategorie(CateElement,isNew);
            IsAllow = true; IsWorking = false;
            await _navigationService.GoBackAsync();
        }

        private void OnRetourCommand()
        {
            _navigationService.GoBackAsync();
        }


        public Categorie CateElement
        {
            get { return _cateElement; }
            set { SetProperty(ref _cateElement, value); }
        }
        public bool IsAllow
        {
            get { return _isAllow; }
            set { SetProperty(ref _isAllow, value); }
        }
        public bool IsWorking
        {
            get { return _isWorking; }
            set { SetProperty(ref _isWorking, value); }
        }


        public void OnNavigatedFrom(NavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("categorie"))
                CateElement = (Categorie)parameters["categorie"];
            else if (parameters.ContainsKey("isNew"))
            {
                isNew = (bool)parameters["isNew"];
                CateElement = new Categorie();
            }
                   
        }
    }
}
