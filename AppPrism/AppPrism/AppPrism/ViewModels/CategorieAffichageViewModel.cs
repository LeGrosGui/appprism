﻿using AppPrism.DAL;
using AppPrism.Model;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppPrism.ViewModels
{
    public class CategorieAffichageViewModel : BindableBase, INavigationAware
    {
        private IRestCategorie rest;
        private INavigationService _navigationService;
        private ObservableCollection<Categorie> _categorieList;
        private bool _isAllow;
        private bool _isWorking;
        private Categorie _selectedCate;




        public DelegateCommand OnNewMovie { get; }
        public DelegateCommand OnMAJData { get; }
        public Command OnItemSelected { get; }
        public DelegateCommand<Categorie> OnEdit { get; }
        public DelegateCommand<Categorie> OnDelete { get; }
        


        public CategorieAffichageViewModel(INavigationService navigationService)
        {
            rest = new RestCategorie();
            IsAllow = true;
            _navigationService = navigationService;
            OnNewMovie = new DelegateCommand(OnNewMovieCommand).ObservesCanExecute((p) => IsAllow);
            //OnMAJData = new DelegateCommand(OnMAJDataCommand);
            OnMAJData = new DelegateCommand(OnMAJDataCommand).ObservesCanExecute((p) => IsAllow);
            OnEdit = new DelegateCommand<Categorie>(OnEditCommand);
            OnDelete = new DelegateCommand<Categorie>(OnDeleteCommand);
            IsWorking = false;
        }


        private void OnNewMovieCommand()
        {
            var p = new NavigationParameters("isNew");
            p.Add("isNew", true);
            _navigationService.NavigateAsync("CategoriePage",p);
        }

        private void OnEditCommand(Object e)
        {
            Categorie cate = e as Categorie;
            var p = new NavigationParameters("categorie");
            p.Add("categorie", cate);
            _navigationService.NavigateAsync("CategoriePage", p);
        }

        private async void OnDeleteCommand(Object e)
        {
            Categorie cate = e as Categorie;
            await rest.DeleteCategorie(cate.ID.ToString());
            CategorieList.Remove(cate);
        }

        private async void OnMAJDataCommand()
        {

            IsWorking = true; IsAllow = false;
            CategorieList = new ObservableCollection<Categorie>(await rest.GetCategorie());
            IsAllow = true; IsWorking = false;
        }

        private void OnItemSelectedCommand(Object e)
        {
            //Categorie cate = e as Categorie;
            //var p = new NavigationParameters("categorie");
            //p.Add("categorie", cate);
            //_navigationService.NavigateAsync("CategoriePage");
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            //OnMAJDataCommand();
        }

        public bool IsAllow
        {
            get { return _isAllow; }
            set { SetProperty(ref _isAllow, value); }
        }
        public bool IsWorking
        {
            get { return _isWorking; }
            set { SetProperty(ref _isWorking, value); }
        }

        private string _test;
        
        public string Test
        {
            get { return _test; }
            set { SetProperty(ref _test, value); }
        }

        public Categorie SelectedCate
        {
            get { return _selectedCate; }
            set
            {
                SetProperty(ref _selectedCate, value);
                OnEditCommand(_selectedCate);
            }
        }

        public ObservableCollection<Categorie> CategorieList
        {
            get { return _categorieList; }
            set { SetProperty(ref _categorieList, value); }
        }

    }
}
