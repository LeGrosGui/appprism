﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AppPrism.ViewModels
{
    public class MainPageViewModel : BindableBase, INavigationAware
    {
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        INavigationService _navigationService;
        public DelegateCommand OnVoirFilm { get; }
        public DelegateCommand OnAuthentication { get; }
        public DelegateCommand<string> Navigate { get; }
        public MainPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            OnAuthentication = new DelegateCommand(OnAuthenticationCommand);
            OnVoirFilm = new DelegateCommand(OnVoirFilmCommand);
            Navigate = new DelegateCommand<string>(NavigateCommand);
        }

        private void OnAuthenticationCommand()
        {
            _navigationService.GoBackAsync();
        }

        private void NavigateCommand(string obj)
        {
            string txt = "AppPrism:///MasterPage/MasterNavigation/" + obj;
            _navigationService.NavigateAsync(txt);
        }

        private async void OnVoirFilmCommand()
        {
            await _navigationService.NavigateAsync("AppPrism:///MasterPage/MasterNavigation/CategorieAffichage");
        }


        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("title"))
                Title = (string)parameters["title"] + " and Prism";
        }
    }
}
