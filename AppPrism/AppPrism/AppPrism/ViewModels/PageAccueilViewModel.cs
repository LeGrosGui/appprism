﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppPrism.ViewModels
{
    public class PageAccueilViewModel : BindableBase, INavigationAware
    {
        public DelegateCommand Navigate { get; }
        INavigationService _navigationService;
        private bool IsBusy { get; set; }
        private bool IsPush { get; set; }

        public PageAccueilViewModel(INavigationService navigationService)
        {
            IsBusy = true;
            _navigationService = navigationService;
            Navigate = new DelegateCommand(NavigationCommand).ObservesCanExecute((p) => IsBusy);
        }

        private void NavigationCommand()
        {
            IsBusy = false;
            if(!IsPush)
                _navigationService.NavigateAsync("AppPrism:///MasterPage/MasterNavigation/FluxRss");
            IsPush = true;
            IsBusy = true;
        }

        private double _imageOpacity;
        public double ImageOpacity
        {
            get { return _imageOpacity; }
            set { SetProperty(ref _imageOpacity, value); }
        }


        private async void setOpacity()
        {
            double i = 0;
            ImageOpacity = 0;

            while (i != 0.99)
            {
                i += 0.01;
                ImageOpacity = i + i;
                await Task.Delay(30);
            }
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            IsPush = false;
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            IsPush = false;
            //setOpacity();
            waitBefore();
            
        }

        private async void waitBefore()
        {
            await Task.Delay(4000);
            if (!IsPush)
                await _navigationService.NavigateAsync("AppPrism:///MasterPage/MasterNavigation/FluxRss");
        }
    }
}
