﻿using AppPrism.Model.RssReaderModel;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;

namespace AppPrism.ViewModels
{
    public class FluxRssViewModel : BindableBase, INavigationAware
    {

        public DelegateCommand LoadRss { get; }
        

        public FluxRssViewModel()
        {
            IsBusy = true;
            FluxRssReader = new RssReader();
            LoadRss = new DelegateCommand(LoadRssCommand).ObservesCanExecute((p) => IsBusy);

        }

        private async void LoadRssCommand()
        {
            IsBusy = false;
            string url = "http://www.jeuxvideo.com/rss/rss-pc.xml";

            await FluxRssReader.LoadItem(url);
            RssTitle = "Jeux Vidéo - PC";
            IsBusy = true;
        }



        private string _rssTitle;
        public string RssTitle
        {
            get { return _rssTitle; }
            set { SetProperty(ref _rssTitle, value); }
        }

        private RssReader _fluxrssReader;
        public RssReader FluxRssReader
        {
            get { return _fluxrssReader; }
            set { SetProperty(ref _fluxrssReader, value); }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private bool _isLoading;
        public bool IsLoading
        {
            get { return _isLoading; }
            set { SetProperty(ref _isLoading, value); }
        }


        //private ObservableCollection<ItemRss> _listItemRss;
        //public ObservableCollection<ItemRss> ListItemRss
        //{
        //    get { return _listItemRss; }
        //    set { SetProperty(ref _listItemRss, value); }
        //}

        private ItemRss _selectedItemRss;
        public ItemRss SelectedItemRss
        {
            get { return _selectedItemRss; }
            set {
                SetProperty(ref _selectedItemRss, value);
                Device.OpenUri(new Uri(SelectedItemRss.Link));
            }
        }

        private ItemRss _lastItem;
        public ItemRss LastItem
        {
            get { return _lastItem; }
            set {
                SetProperty(ref _lastItem, value);
                loadMore(_lastItem, FluxRssReader.ListItemRss);
            }
            
        }


        public async void loadMore(ItemRss LastItem, ObservableCollection<ItemRss> ListItems)
        {
            if (IsLoading || ListItems.Count == 0)
            {

            }
            if (LastItem.Id == ListItems.Count - 1)
            {
                IsLoading = true;
                await FluxRssReader.LoadMore(LastItem.Id);
                IsLoading = false;
            }
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            LoadRssCommand();
        }
    }
}
