﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Windows.Data.Xml.Dom;

namespace AppPrism.Model.RssReaderModel
{
    public class RssReader
    {
        public string ReaderTitle { get; set; }
        public string ReaderDescription { get; set; }
        public ObservableCollection<ItemRss> ListItemRss { get; set; }
        public string Url { get; set; }

        public RssReader()
        {
            ListItemRss = new ObservableCollection<ItemRss>();
        }


        public async Task LoadItem(string feed)
        {
            
            try
            {
                var httpClient = new HttpClient();
                Url = feed;
                var responseString = await httpClient.GetStringAsync(Url);
                ListItemRss.Clear();
                var items = await ParseFeed(responseString);
                foreach (var item in items)
                {
                    ListItemRss.Add(item);
                }
            }
            catch (Exception e)
            {
                ListItemRss.Clear();
                ListItemRss.Add(new ItemRss { Title="PAS DE CONNEXION INTERNET"});
            }
            
        }


        public async Task LoadMore(int lastID)
        {
            var httpClient = new HttpClient();
            var responseString = await httpClient.GetStringAsync(Url);

            ListItemRss.Clear();

            var items = await ParseFeed(responseString);
            int i = 0;
            int ii = 0;
            foreach (var item in items)
            {
                if(i <= lastID)
                    if (ii < 10)
                    {
                        ListItemRss.Add(item);
                        ii++;
                    }
                    
            }
        }





        private async Task<List<ItemRss>> ParseFeed(string rss)
        {
            return await Task.Run(() =>
            {
                var xdoc = XDocument.Parse(rss);
                var id = 0;
                return (from item in xdoc.Descendants("item")
                        select new ItemRss
                        {
                            Title = (string)item.Element("title"),
                            Description = (string)item.Element("description"),
                            Link = (string)item.Element("link"),
                            PubDate = (DateTime)item.Element("pubDate"),
                            Guid = (string)item.Element("guid"),
                            Id = id++
                        }).ToList();

            });
        }

        public ItemRss GetFeedItem(int id)
        {
            return ListItemRss.FirstOrDefault(i => i.Id == id);
        }


    }
}
