﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppPrism.Model.RssReaderModel
{
    public class ItemRss
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public DateTime PubDate { get; set; }
        public string Guid { get; set; }
        public string Enclosure { get; set; }
        public int Id { get; set; }
    }
}
