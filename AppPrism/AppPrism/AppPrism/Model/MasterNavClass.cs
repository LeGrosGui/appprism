﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppPrism.Model
{
    public class MasterNavClass
    {
        public string Name { get; set; }
        public string ImageSource { get; set; }
        public string Destination { get; set; }
    }
}
